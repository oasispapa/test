FROM python:3.11
WORKDIR /code
COPY ./requirements.txt /code/requirements.txt

# cache 를 사용해서 패키지 설치 시간을 줄인다.
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

# 가장 마지막에 캐시를 사용할 수 없고 가장 빈번하게 변경되는 부분을 복사한다.
COPY . /code/app
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8888"]
